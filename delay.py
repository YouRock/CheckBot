from telebot.types import ReplyKeyboardRemove
from bot import bot
import time

def delayCheck(obj):
    while obj.delayCheck:
        time.sleep(0.1)
        if time.time() - obj.timePastCheck > 30:
            bot.send_message(obj.id, 'Мне нужен ваш ответ. Напишите "да" или "нет".')
            break
    while obj.delayCheck:
        time.sleep(0.1)
        if time.time() - obj.timePastCheck > 210:
            bot.send_message(obj.id, 'Давайте начнём сначала', reply_markup=ReplyKeyboardRemove())
            break
    obj.state = 'chat'

def delayForm(obj):
    while obj.delayForm:
        time.sleep(0.1)
        if time.time() - obj.timePastCheck > 30:
            bot.send_message(obj.id, "Я все еще хочу вам помочь. Попробуйте перефразировать вопрос")
            break
    while obj.delayForm:
        time.sleep(0.1)
        if time.time() - obj.timePastCheck > 210:
            bot.send_message(obj.id, 'Давайте начнём сначала', reply_markup=ReplyKeyboardRemove())
            break
    obj.state = 'chat'
