import re
from nltk.corpus import stopwords
import nltk
import pymorphy2

def normalize(text):
    text = re.findall('\w+', text)
    m = pymorphy2.MorphAnalyzer()

    ans=[]
    for w in text:
        ans.append(m.parse(w)[0].normal_form)

    ans = [x for x in ans if x !=' ' and x != '\n']
    stop = stopwords.words('russian')

    res = []
    for w in ans:
        if w not in stop:
            res.append(w)

    return ' '.join(res)

def tokenization(text):
    return nltk.sent_tokenize(text, language='russian')

