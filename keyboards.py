import telebot

def choose_class_number():
    """
    Возвращает клавиатуру для выбора цифры класса
    :return: telebot.types.ReplyKeyboardMarkup
    """
    keyboard = telebot.types.ReplyKeyboardMarkup(True)
    keyboard.row('5', '6', '7', '8')
    keyboard.row('9', '10', '11', 'справка')
    return keyboard

def choose_class_letter():
    """
    Возвращает клавиатуру для выбора буквы класса
    :return: telebot.types.ReplyKeyboardMarkup
    """
    keyboard = telebot.types.ReplyKeyboardMarkup(True)
    keyboard.row('А', 'Б')
    keyboard.row('В', 'Г')
    return keyboard

def choose_yas_or_no():
    """
    Возвращает клавиатуру с выбором да или нет
    :return: telebot.types.ReplyKeyboardMarkup
    """
    keyboard = telebot.types.ReplyKeyboardMarkup(True)
    keyboard.row('да', 'нет')
    return keyboard

def listOfThemesKeyB(list):
    keyboard = telebot.types.ReplyKeyboardMarkup(True)

    l = []
    i=1
    while i <= len(list):
        l.append(str(i))
        i+=1

    keyboard.row(*l)
    return keyboard

def operators():
    """
    Возвращает клавиатуру с возможнастями
    :return: telebot.types.ReplyKeyboardMarkup
    """
    keyboard = telebot.types.ReplyKeyboardMarkup(True)
    keyboard.row('Узнать расписание на завтра')
    keyboard.row('Выбрать другой класс')
    return keyboard