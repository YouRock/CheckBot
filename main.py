import logging
import pandas as pd
import time
import requests
from bot import bot
from users import User# Класс описания пользователя
from ml import init_theme
from ml import get_csv_ans

from config import *

logging.basicConfig(level=logging.DEBUG,filename='logs.log')
#print('init start')
init_theme()
#print('init end')

clients = dict()

@bot.message_handler(content_types=['document'])
def func(message):
    id = message.chat.id
    file_info = bot.get_file(message.document.file_id)
    file = requests.get('https://api.telegram.org/file/bot{0}/{1}'.format(TOKEN, file_info.file_path))
    with open("in.csv", "wb") as code:
        code.write(file.content)
        #реализуй обработку
        dt = pd.read_csv('in.csv', encoding='utf-8')
        ans = get_csv_ans(dt)
        ans.to_csv('out.csv', index=False, encoding='utf-8')
        doc = open('out.csv','rb')
        bot.send_document(id, doc)

@bot.message_handler(content_types=['text'])
def main_handler(message):
    logging.info('text: '+message.text+' \ninfo: '+str(message))
    id = message.chat.id
    if id not in clients.keys():
        clients[id] = User(message)
    bot.send_message(id, clients[id].get_response(message),reply_markup=clients[id].keyboard)

while True:
    try:
        bot.polling(none_stop=True, timeout=60, interval=5)
    except Exception as e:
        logging.error(e)
        time.sleep(10)
