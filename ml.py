import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.ensemble import VotingClassifier, RandomForestClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC, SVC
from sklearn.tree import DecisionTreeClassifier
from func import normalize
import pickle

MEGA_CONST = 0.5
MIN_CONST = 0.43

clf2 = Pipeline([('vect', CountVectorizer(ngram_range=(1, 2))),
                 ('tfidf', TfidfTransformer(use_idf=False)),
                 ('svm', OneVsRestClassifier(LinearSVC()))])

clf = Pipeline([('vect', TfidfVectorizer(ngram_range=(1, 2), use_idf=False)),
                ('svm',
                 VotingClassifier([('Log', LogisticRegression(C=11)),
                                   ('RFC', RandomForestClassifier(n_estimators=45, n_jobs=-1)),
                                   ('OVRC', OneVsRestClassifier(SVC(probability=True)))], voting='soft')
                 )])
themes = pd.read_csv('themes.csv', encoding='utf-8', header=None).iloc[:, 1]
train_data = pd.read_csv('train_norm.csv', encoding='utf-8').dropna()


# test_data = pd.read_csv('test_norm.csv', encoding='utf-8')

def init_theme():
    global clf
    X_train = train_data['Speech']
    y_train = train_data['ThemeLabel']
    # X_test = test_data['Speech'])
    clf = pickle.load(open('clf.pickle', 'rb'))
    # clf = clf.fit(X_train, y_train)
    # pickle.dump(clf, open('clf.pickle','wb'))


def get_csv_ans(data):
    global clf2
    clf2 = clf2.fit(train_data['Speech'], train_data['ThemeLabel'])
    ttt = clf2.predict(data)
    ans = pd.DataFrame(columns=['Index', 'ThemeLabel'])
    ans['ThemeLabel'] = ttt
    ans['Index'] = data['Index']
    return ans


def get_theme(text):
    global clf
    text = text.lower()
    text = normalize(text)
    tt = clf.predict_proba([text])[0]
    tt2 = np.argsort(tt)
    if (tt[tt2[-1]] <= MIN_CONST):
        return None
    ans = []
    for i in reversed(tt2):
        if (tt[i] + MEGA_CONST >= tt[tt2[-1]]):
            ans.append(themes[i])
    return ans