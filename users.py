from ml import get_theme
from telebot.types import ReplyKeyboardRemove
import keyboards
import threading
import delay
import time

class User:
    # Конструктор
    def __init__(self, message):
        self.id = message.chat.id
        self.keyboard = ReplyKeyboardRemove()
        self.state = 'chat'
        self.theme = ""
        self.countAppeal = 0
        self.countList = 0

    # Основной мметод

    def showThemes(self): #отдебож
        ret = ''
        i = 1
        for s in self.theme:
            ret = ret + str(i) + ') ' + s + '\n'
            i += 1

        #ret = ret + chr(i)+') Никакая из предложенных.\n'
        return ret

    def get_response(self, message):
        text = message.text.lower()
        self.keyboard = ReplyKeyboardRemove()

        if text == '/start':
            return "Здравствуйте!\n" \
                   "Это бот для работы с клиентами банка.\n" \
                   "Напишите, что вас интересует."

        if self.state=='list':
            if text.isdigit():
                if int(text) in range(1, len(self.theme)):
                    return 'В ближайшее время на ваш запрос ответит оператор.'
                elif int(text) == len(self.theme):
                    if self.countList == 2:
                        self.countList=0
                        return "ОК, похоже, мы оба запутались, давайте начнем заново :)"

                    self.countList+=1
                    return 'Не смогли определить тему вашего вопроса. Попробуйте перефразировать вопрос.'
                else:
                    return 'Номер темы указан не правильно. Уточните, какая тема вас интересует:\n'+self.showThemes()
            else:
                self.state='chat' #перейдет в чат, если новый вопрос

        if self.state == 'chat': #саше надо передать, что польз перефраз вопр
            self.theme = get_theme(text)

            if type(self.theme) == type(list()) and len(self.theme)>1:

                self.theme = self.theme[:3]
                self.theme.append('Никакая из предложенных.')
                ret='Пожалуйста, уточните, какая тема вас интересует:\n'
                self.state='list'

                self.keyboard=keyboards.listOfThemesKeyB(self.theme)
                return ret+self.showThemes()

            else:
                if (self.theme != None):
                    self.state = 'check'
                    self.keyboard = keyboards.choose_yas_or_no()
                    #Пораждаю поток
                    self.timePastCheck = time.time()
                    self.delayCheck = True
                    t = threading.Thread(target=delay.delayCheck, args=(self,))
                    t.daemon = True
                    t.start()

                    return 'Вас интересует ' + ''.join(self.theme) + '. Да?'
                else:
                    return "Не финансовая тематика \u263A"

        if self.state == 'check': #правильно ли определил тему бот
            self.state = 'chat'
            self.delayForm = False
            self.delayCheck = False
            self.timePastCheck = time.time()
            if text in ("да","конечно","ага","угадал","точно","верно"):
                self.countAppeal = 0
                if self.theme == 'Курс доллара':
                    return 'Текущий курс доллара: 65 рублей. В ближайшее время на ваш запрос ответит оператор.'
                elif self.theme == 'Курс евро':
                    return 'Текущий курс евро: 70 рублей. В ближайшее время на ваш запрос ответит оператор.'
                else:
                    return 'В ближайшее время на ваш запрос ответит оператор.'
            elif text in ('нет',"неа","не","ошибаешься","ошибка"):
                self.countAppeal+=1
                if self.countAppeal ==3:
                    self.countAppeal = 0
                    return "ОК, я запутался, давайте начнем заново :)"
                #Пораждаю поток ожидания
                self.timePastForm = time.time()
                self.delayForm = True
                t = threading.Thread(target=delay.delayForm, args=(self,))
                t.daemon = True
                t.start()
                return 'Не смогли определить тему вашего вопроса. Попробуйте перефразировать вопрос.'
            else:
                self.delayCheck = False
                time.sleep(0.2)
                return self.get_response(message)